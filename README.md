Python basictool Module
=======================

Introduction
------------

basictool allows to wrap Python functions into shell script commands.
 
**useless-script.py**

    #!/usr/bin/env python

    import basictool
        
    @basictool.expose_action
    def some_useless_action(some_argument):
        return "I'm heck'a useless. You passed me %s." % some_argument
        
    def main():
        basictool.init()
        basictool.run()
        basictool.finalize()
        
    if __name__ == "__main__":
        main()



Now let's run from the shell:

    $ ./useless-script.py some-useless-action "some useless argument"
    
    I'm heck'a useless. Here's an argument you passed: some useless argument
    

Advanced Examples
-----------------

Consider the following Python function in extractor.py file (notice the positional arguments):

**extractor.py**

    #!/usr/bin/env python
    import basictool
    
    @basictool.expose_action
    def body(line_start, line_end, *files):  # <-- Function 'body' will be exposed as action 'body' of extractor.py script.
        """
        Reads lines starting from line_start, until line_end is reached (not included in the output).
        """
        global print_line_numbers
    
        result = ""
    
        for file_name in files:
            with open(file_name, 'r') as fd:
                lines = fd.readlines()
                if not print_line_numbers:
                    formatted_lines = ''.join(
                        ["%s" % line for i, line in enumerate(lines) if i >= int(line_start) and i < int(line_end)]
                    )
                else:
                    formatted_lines = ''.join(
                        ["%i %s" % (i, line) for i, line in enumerate(lines) if i >= int(line_start) and i < int(line_end)]
                    )
    
        return formatted_lines  # <-- The returned value will be formatted and printed out by basictool.

Running the following from the command line:

    $ extractor.py body 1 10 README.md
    
...will result with the following output:

    =======================
    
    Introduction
    ------------
    
    basictool allows to wrap Python functions into shell script commands.
    
    Consider the following Python function in extractor.py file:
    
Help is also generated, based on the docstring in the Python function:

    $ extractor.py body --help
    
    usage: extractor-example.py body [-h] line_start line_end [files [files ...]]
    
    Reads lines starting from line_start, until line_end is reached (not included
    in the output).
    
    positional arguments:
      line_start
      line_end
      files
    
    optional arguments:
      -h, --help  show this help message and exit
      
A list of available actions is available through --help switch:

    $ extractor.py --help
    
    usage: extractor-example.py [-h] [--print-line-numbers]
                            {body,head,stats,tail} ...
    
    positional arguments:
      {body,head,stats,tail}
        body                Reads lines starting from line_start, until line_end
                            is reached (not included in the output).
        head                Reads first line_count lines from the file in files.
        stats               Counts the number of total lines, and the number of
                            empty lines in each file. Output: TOTAL_LINES_COUNT
                            TOTAL_EMPTY_LINES_COUNT
        tail                Reads last line_count lines from the file in files.
    
    optional arguments:
      -h, --help            show this help message and exit
      --print-line-numbers

Python functions can also return lists or tuples, which will be joined with spaces:

    @basictool.expose_action
    def stats(*files):
        """
        Counts the number of total lines, and the number of empty lines in each file.
        Output: TOTAL_LINES_COUNT TOTAL_EMPTY_LINES_COUNT
        """
        total_lines_count = 0
        total_empty_lines_count = 0
    
        for file_name in files:
            with open(file_name, 'r') as fd:
                lines = fd.readlines()
                total_lines_count += len(lines)
                empty_lines = [line for line in lines if line.strip() == ""]
                total_empty_lines_count += len(empty_lines)
    
        return total_lines_count, total_empty_lines_count  # <-- Return value will be formatted and printed out by basictool.

Now let's count the lines in the README.md file:

    $ extractor.py stats README.md
    
    111 33
    
111 lines total, with 33 empty lines.

The examples above have the body of the main function of the Python script looking like this:

    def main():
    
        global print_line_numbers
    
        # Initialize the basictool framework and read the global arguments:
        global_arguments = basictool.init(
            {'print_line_numbers': False}
        )
        # Store the parsed global arguments:
        print_line_numbers = global_arguments['print_line_numbers']
    
        basictool.run()
        basictool.finalize()  # <-- Exits the tool with the corresponding exit status code.
    
    if __name__ == "__main__":
        main()
        
'print_line_numbers' global variable can be used in the following manner (first line in file has number 0):

    $ extractor.py --print-line-numbers body 1 10 README.md
    
    1 =======================
    2 
    3 Introduction
    4 ------------
    5 
    6 basictool allows to wrap Python functions into shell script commands.    
    7 
    8 Consider the following Python function in extractor.py file:
    9 
    
Formatting Output for Shell Parsing
-----------------------------------

Sometimes you may want to parse the output of a basictool-wrapped command into a bash array. basictool outputs the
array return values from exposed functions as multiple lines (array items joined by \n character).

Consider the following example:

**myprgog.py**
    
    #!/usr/bin/env python
    
    import basictool
    
    @basictool.expose_action
    def array_output():
        return ["item1", "item 2", "$item \"3\""]
        
    def main():
        basictool.init()
        basictool.run()
        basictool.finalize()
        
...will generate the following basictool output:
    
    $ ./myprog.py array-output
    
    item1
    item 2
    $item "3"
    
This allows bash to parse the output into an array very easily (requires setting the field delimiter to \n character):

    #!/bin/bash
    
    IFS=$'\n' # <-- Set the field delimiter.
    
    foo=( $( ./myprog.py array-output ) ) # <-- Read the output from the Python script.
    
    echo "foo[0] = ${foo[0]}"
    echo "foo[1] = ${foo[1]}"
    echo "foo[2] = ${foo[2]}"
    
The output of the bash script above will be:

    foo[0] = item1
    foo[1] = item 2
    foo[2] = $item "3"


Loading Configuration Files
---------------------------

Configuration file path in can be passed in the argument `--config-file`, if `allow_config_file` is set to `True`
when calling the `init` function. File must be in YAML format. Configuration parameters stored in such file will be
returned by the `init` as 'global arguments'. 

If `--config-file` is not specified, but `allow_config_file` is `True`, then a configuration file search attempt will
be made in the following priority order:

- ./\<program_name\>.config.yml
- ~/.\<program_name\>.config.yml

...where \<program_name\> is the file name (no extension) portion of the `sys.argv[0]`.

The 'extractor-example.py' example above will attempt to look for files:

- ./extractor-example.config.yml
- ~/.extractor-example.config.yml
