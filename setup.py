__author__ = 'Alex Prykhodko (Pixellu LLC)'

from distutils.core import setup
import basictool

setup(
    name=basictool.__name__,
    version=basictool.__version__,
    description="A framework for building simple CLI tools.",
    author="Alex Prykhodko",
    author_email="alex@pixellu.com",
    py_modules=[basictool.__name__],
    install_requires=['PyYAML']
)
