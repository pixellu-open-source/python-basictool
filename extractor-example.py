#!/usr/bin/env python

__author__ = 'Alex Prykhodko (Pixellu LLC)'

import basictool

print_line_numbers = False


@basictool.expose_action
def head(line_count, *files):
    """
    Reads first line_count lines from the file in files.
    """
    global print_line_numbers

    formatted_lines = ""

    for file_name in files:
        with open(file_name, 'r') as fd:
            lines = fd.readlines()
            if not print_line_numbers:
                formatted_lines = ''.join(
                    ["%s" % line for i, line in enumerate(lines) if i < int(line_count)]
                )
            else:
                formatted_lines = ''.join(
                    ["%i %s" % (i, line) for i, line in enumerate(lines) if i < int(line_count)]
                )

    return formatted_lines  # <-- Return value will be formatted and printed out by basictool.


@basictool.expose_action
def tail(line_count, *files):
    """
    Reads last line_count lines from the file in files.
    """
    global print_line_numbers

    formatted_lines = ""

    for file_name in files:
        with open(file_name, 'r') as fd:
            lines = fd.readlines()
            if not print_line_numbers:
                formatted_lines = ''.join(
                    ["%s" % line for i, line in enumerate(lines) if i > len(lines) - int(line_count)]
                )
            else:
                formatted_lines = ''.join(
                    ["%i %s" % (i, line) for i, line in enumerate(lines) if i > len(lines) - int(line_count)]
                )
    return formatted_lines  # <-- Return value will be formatted and printed out by basictool.


@basictool.expose_action
def body(line_start, line_end, *files):
    """
    Reads lines starting from line_start, until line_end is reached (not included in the output).
    """
    global print_line_numbers

    formatted_lines = ""

    for file_name in files:
        with open(file_name, 'r') as fd:
            lines = fd.readlines()
            if not print_line_numbers:
                formatted_lines = ''.join(
                    ["%s" % line for i, line in enumerate(lines) if i >= int(line_start) and i < int(line_end)]
                )
            else:
                formatted_lines = ''.join(
                    ["%i %s" % (i, line) for i, line in enumerate(lines) if i >= int(line_start) and i < int(line_end)]
                )

    return formatted_lines  # <-- Return value will be formatted and printed out by basictool.

@basictool.expose_action
def stats(*files):
    """
    Counts the number of total lines, and the number of empty lines in each file.
    Output: TOTAL_LINES_COUNT TOTAL_EMPTY_LINES_COUNT
    """
    total_lines_count = 0
    total_empty_lines_count = 0

    for file_name in files:
        with open(file_name, 'r') as fd:
            lines = fd.readlines()
            total_lines_count += len(lines)
            empty_lines = [line for line in lines if line.strip() == ""]
            total_empty_lines_count += len(empty_lines)

    return total_lines_count, total_empty_lines_count  # <-- Return value will be formatted and printed out by basictool.


@basictool.expose_action
def array_output(max_items=3):
    """
    Returns a maxium of 3 array items.
    Command line: ./extractor-example.py array-output --max-items 3
    """
    max_items = int(max_items)
    if max_items > 3:
        raise Exception("3 or less items are supported.")
    return ["item1", "item 2", "$item \"3\""][0:max_items]


@basictool.expose_action
def false():
    """
    A simple function that returns False.
    """
    return False


@basictool.expose_action
def true():
    """
    A simple function that returns True.
    """
    return True


def main():

    global print_line_numbers

    # Initialize the basictool framework and read the global arguments:
    global_arguments = basictool.init(
        {'print_line_numbers': False}
    )
    # Store the parsed global arguments:
    print_line_numbers = global_arguments['print_line_numbers']

    basictool.run()
    basictool.finalize()  # <-- Exits the tool with the corresponding exit status code.

if __name__ == "__main__":
    main()