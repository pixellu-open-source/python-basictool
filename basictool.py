__author__ = 'Alex Prykhodko (Pixellu LLC)'
__version__ = "0.3.0"

import argparse
import unittest
import inspect
import os.path
import yaml
import re
import logging

_parser = None
_allow_config_file = None
_parsed_action_function = None
_parsed_action_function_args = []
_parsed_global_arguments = {}
_configuration = {}
_output = None

_CONFIG_FILE_NAME_SUFFIX = '.config.yml'

class expose_action(object):
    """
    Decorator for exposing CLI actions.
    """

    exposed_actions = {}

    def __init__(self, f):
        arg_spec = inspect.getargspec(f)
        if arg_spec.keywords is not None:
            raise Exception("Exposing functions with kwargs is not supported.")
        func_name = f.__name__.replace('_', '-')
        expose_action.exposed_actions[func_name] = f
        self.f = f

    def __call__(self, *args, **kwargs):
        self.f(*args, **kwargs)

#==============================================================================


def _get_default_args(func):
    """
    Returns a dictionary of arg_name: default_values for the input function.
    """
    args, varargs, keywords, defaults = inspect.getargspec(func)
    return dict(list(zip(args[-len(defaults):], defaults)))


def _format_output(value):
    """
    Quotes the argument for bash output (escapes quotes, adds quotes when space characters are detected).
    Such output can be parsed into an array.
    @param value: Can be one of the following:
                  - a single item in of type str, int, float, bool, unicode
                  - a list of mixed item types: str, int, float, bool, unicode
    @return: a string, with all items in value quoted and escaped.
    """

    assert type(value) in (str, int, float, bool, str, list, tuple, None.__class__)

    result = []
    items = []

    if type(value) in (str, int, float, bool, str):
        items = [str(value)]
    elif type(value) in (list, tuple):
        items = value
    elif value is None:
        return None
    if len(items) > 0:
        for item in items:
            assert type(item) in (str, int, float, bool, str, None.__class__)
            new_item = str(item)
            result.append(new_item)

    return "\n".join(result)


def _variable_name_to_argument_name(variable_name):
    argument_name = variable_name.replace('_', '-')
    argument_name = re.sub('(^-+|-+$)', '', argument_name)
    argument_name = re.sub('-{1,}', '-', argument_name)
    return argument_name

#==============================================================================


def _load_configuration(config_file_path, parsed_global_arguments):
    """
    Loads the configuration file specified by the config_file_path argument.
    processes the parsed CLI arguments, and resolves the final list of argument values taking into account
    the provided configuration file.
    Global arguments override the configuration file values, unless the global argument value is none.
    @param config_file_path: path to the configuration file to load.
    @param parsed_global_arguments: resolved global CLI arguments with their default values.
    @return: a dictionary, containing the parameters from the configuration file, resolved with the parsed global arguments.
    """

    logging.debug("Loading config file from '%s' ..." % config_file_path)
    resolved_global_arguments = {}
    # Load the configuration file from the path specified in 'config_file' argument:
    if os.path.exists(config_file_path):
        with open(config_file_path, 'r') as fp:
            resolved_global_arguments = yaml.load(fp)
    else:
        raise IOError("Configuration file '%s' not found" % (config_file_path))

    # Resolve the global/configuration arguments:
    for key in parsed_global_arguments:
        if key == 'config_file':
            continue
        if parsed_global_arguments[key] is not None and key in resolved_global_arguments:
            resolved_global_arguments[key] = parsed_global_arguments[key]
        elif key not in resolved_global_arguments:
            resolved_global_arguments[key] = parsed_global_arguments[key]

    return resolved_global_arguments


def _look_up_config_file(program_name):
    """
    Searches the file system for the configuration file for program_name. First searches in current directory, then in
    home directory. Returns None if configuration file is not found.
    @param program_name:
    @return:
    """

    # TODO: Write unit tests for config file look up.

    config_file_name = os.path.splitext(program_name)[0] + _CONFIG_FILE_NAME_SUFFIX

    config_file_path = "./%s" % config_file_name
    if os.path.isfile(config_file_path):
        return config_file_path

    config_file_path = os.path.expanduser("~/.%s" % config_file_name)
    if os.path.isfile(config_file_path):
        return config_file_path

    return None


def _parse_arguments(global_arguments=None, cli_arguments=None, allow_config_file=False):
    """
    Parses CLI arguments based on the exposed action functions, resolves the default values of the global
    arguments.
    @param global_arguments: a dictionary, argument names as dictionary keys,
                             and their default values as dictionary values.
    @param cli_arguments: list of CLI arguments that need to be parsed, if they are not being parsed from sys.argv.
    @param allow_config_file: Parses the --config-file argument, and returns as part of the dictionary.
    @return: a tuple, containing:
             - action function
             - action function arguments (resolved with the function's default values)
             - global arguments (resolved with defaults)
    """
    global _parser, _allow_config_file

    _parser = argparse.ArgumentParser()

    _allow_config_file = allow_config_file
    if _allow_config_file:
        _parser.add_argument('--config-file', help="YAML-formatted configuration file to load.")

    # Add global arguments as optional arguments:
    if global_arguments is not None:
        assert type(global_arguments) is dict
        for global_argument_name in global_arguments:
            assert type(global_arguments[global_argument_name]) in (str, int, float, bool, str, None.__class__)
            if type(global_arguments[global_argument_name]) is not bool:
                # For types other than bool, store the parsed value:
                _parser.add_argument('--' + _variable_name_to_argument_name(global_argument_name),
                                    default=global_arguments[global_argument_name])
            else:
                # Use 'store_true' action for bool types:
                _parser.add_argument('--' + _variable_name_to_argument_name(global_argument_name),
                                    action='store_true',
                                    default=global_arguments[global_argument_name])

    subparsers = _parser.add_subparsers()

    action_function_args = []

    # Iterate over all of the exposed actions (exposed using the @expose_action decorator):
    for exposed_function_name in expose_action.exposed_actions:
        exposed_function = expose_action.exposed_actions[exposed_function_name]
        exposed_function_arg_spec = inspect.getargspec(exposed_function)
        new_parser = subparsers.add_parser(exposed_function_name,
                                           help=exposed_function.__doc__,
                                           description=exposed_function.__doc__)
        # Set the action function for the current subparser:
        new_parser.set_defaults(action_function=exposed_function)
        exposed_function_defaults = {}
        if exposed_function_arg_spec.defaults is not None:
            exposed_function_defaults = _get_default_args(exposed_function)
        for attr_name in exposed_function_arg_spec.args:
            if attr_name in exposed_function_defaults:
                new_parser.add_argument('--' + _variable_name_to_argument_name(attr_name),
                                        default=exposed_function_defaults[attr_name],
                                        help="Default value: %s" % str(exposed_function_defaults[attr_name]))
            else:
                new_parser.add_argument(attr_name)

        if exposed_function_arg_spec.varargs is not None:
            new_parser.add_argument(exposed_function_arg_spec.varargs, nargs='*')

    # Attempt to parse the cli_arguments argument, instead of the actual sys.argv list:
    if cli_arguments is not None:
        assert type(cli_arguments) is list
        arguments = _parser.parse_args(cli_arguments)
    else:
        arguments = _parser.parse_args()

    if not hasattr(arguments, 'action_function'):
        raise argparse.ArgumentError("Action function not specified.")

    action_function = arguments.action_function
    action_function_arg_spec = inspect.getargspec(action_function)

    # Parse positional arguments:
    for attr in action_function_arg_spec.args:
        action_function_args.append(getattr(arguments, attr))

    # Parse optional positional arguments:
    if action_function_arg_spec.varargs is not None\
            and hasattr(arguments, action_function_arg_spec.varargs)\
            and getattr(arguments, action_function_arg_spec.varargs) is not None:
        for arg in getattr(arguments, action_function_arg_spec.varargs):
            action_function_args.append(arg)

    parsed_global_arguments = {}
    if hasattr(arguments, 'config_file'):
        parsed_global_arguments['config_file'] = arguments.config_file
    if global_arguments is not None:
        for global_argument_name in global_arguments:
            if hasattr(arguments, global_argument_name):
                parsed_global_arguments[global_argument_name] = getattr(arguments, global_argument_name)

    return (action_function, action_function_args, parsed_global_arguments)

#==============================================================================


def init(global_arguments_defaults=None, allow_config_file=False):
    """
    Initializes the basictool by reading the CLI arguments, configuration file, and
    resolving all of the detected parameters.
    Returns the resolved arguments and configuration file parameters.
    @param global_arguments_defaults: dictionary containing the default values of the global CLI arguments.
    @param allow_config_file: Set to True to read and parse YAML-formatted configuration file specified in --config-file CLI argument.
    @return: resolved CLI arguments dictionary and configuration file parameters.
    """
    global _parsed_action_function, _parsed_action_function_args, _parsed_global_arguments
    (_parsed_action_function, _parsed_action_function_args, _parsed_global_arguments) =\
        _parse_arguments(global_arguments_defaults, allow_config_file=allow_config_file)

    resolved_global_arguments = _parsed_global_arguments

    if _allow_config_file:
        config_file_path = _look_up_config_file(_parser.prog)
        if _parsed_global_arguments['config_file'] is not None:
            resolved_global_arguments = _load_configuration(_parsed_global_arguments['config_file'], _parsed_global_arguments)
        elif config_file_path is not None:
            resolved_global_arguments = _load_configuration(config_file_path, _parsed_global_arguments)

    return resolved_global_arguments


def run(should_print=True):
    """
    Runs the action function detected in the CLI arguments, and prints out the return of that function by default.
    Only one action function can be run.
    Returns the output of the action function that had been called.
    @param should_print: Set to False if action function output should not be printed, but only returned.
    @return: the output of the action function that had been called.
    """
    global _parsed_action_function, _parsed_action_function_args, _output

    # Execute the action function (found in the arguments):
    _output = _parsed_action_function(*_parsed_action_function_args)
    _output_str = _format_output(_output)

    if _output_str is not None and should_print:
        print(_output_str)

    return _output

def finalize(none_output_exit_status=1, false_output_exit_status=1):
    """
    Exits the tool with a corresponding exit status.
    By default, if the action function output was None, then exit status will be 1.
    @param none_output_exit_status: exit status when the output value was None.
    @return: does not return anything.
    """
    global _output

    if _output is None:
        exit(none_output_exit_status)
    elif type(_output) is bool and not _output:
        exit(false_output_exit_status)
    else:
        exit(0)


#==============================================================================
# UNIT TESTS
#==============================================================================

class BasicToolTests(unittest.TestCase):

    def test_helpers(self):

        result = _format_output(10)
        self.assertEqual('10', result)

        result = _format_output(11.0)
        self.assertEqual('11.0', result)

        result = _format_output('item 1')
        self.assertEqual('item 1', result)

        result = _format_output(['item1'])
        self.assertEqual('item1', result)

        result = _format_output(['item1', 'item2'])
        self.assertEqual('item1\nitem2', result)

        result = _format_output(['item1', 'item 2'])
        self.assertEqual('item1\nitem 2', result)

        result = _format_output(['item1', 'item 2', '"item 3"'])
        self.assertEqual('item1\nitem 2\n"item 3"', result)

        result = _format_output(['item1', '$item 2', '"item 3"'])
        self.assertEqual('item1\n$item 2\n"item 3"', result)

        result = _format_output((1, 2, 3, "item 4"))
        self.assertEqual('1\n2\n3\nitem 4', result)

        result = _format_output((1, 2, 3, "item 4"))
        self.assertEqual('1\n2\n3\nitem 4', result)

        result = _variable_name_to_argument_name('var_name')
        self.assertEqual('var-name', result)

        result = _variable_name_to_argument_name('_var_name')
        self.assertEqual('var-name', result)

        result = _variable_name_to_argument_name('__var_name_')
        self.assertEqual('var-name', result)

        result = _variable_name_to_argument_name('var___name__')
        self.assertEqual('var-name', result)

    def test_cli_arguments(self):

        # BEGIN functions used in the unit test:

        @expose_action
        def testFunc1(issue_key):
            pass

        @expose_action
        def testFunc2(issue_key, *args):
            pass

        @expose_action
        def testFunc3(*args):
            pass

        @expose_action
        def testFunc4(arg1='testArgument1Default'):
            pass

        @expose_action
        def testFunc5(issue_key, arg2='testArgument2Default'):
            pass

        @expose_action
        def testFunc6(arg1='testArgument1Default', arg2='testArgument2Default'):
            pass

        # END test function definitions.


        # Test various use cases:

        global_arguments = {
            'user': None,
            'password': 'strongpassword'
        }

        cli_arguments = ['testFunc1', 'ISS-1']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(global_arguments, cli_arguments)
        self.assertEqual('testFunc1', parsed_action_function.__name__)
        self.assertEqual(['ISS-1'], parsed_action_function_args)
        self.assertEqual(global_arguments['user'], parsed_global_arguments['user'])
        self.assertEqual(global_arguments['password'], parsed_global_arguments['password'])

        cli_arguments = ['--user', 'testUser', 'testFunc1', 'ISS-1', ]
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(global_arguments, cli_arguments)
        self.assertEqual('testFunc1', parsed_action_function.__name__)
        self.assertEqual(['ISS-1'], parsed_action_function_args)
        self.assertEqual('testUser', parsed_global_arguments['user'])

        cli_arguments = ['testFunc1', 'ISS-2']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc1', parsed_action_function.__name__)
        self.assertEqual(['ISS-2'], parsed_action_function_args)

        cli_arguments = ['testFunc2', 'ISS-3']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc2', parsed_action_function.__name__)
        self.assertEqual(['ISS-3'], parsed_action_function_args)

        cli_arguments = ['testFunc2', 'ISS-4', 'testArgument']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc2', parsed_action_function.__name__)
        self.assertEqual(['ISS-4', 'testArgument'], parsed_action_function_args)

        cli_arguments = ['testFunc3', 'testArgument1', 'testArgument2']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc3', parsed_action_function.__name__)
        self.assertEqual(['testArgument1', 'testArgument2'], parsed_action_function_args)

        cli_arguments = ['testFunc4']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc4', parsed_action_function.__name__)
        self.assertEqual(['testArgument1Default'], parsed_action_function_args)

        cli_arguments = ['testFunc4', '--arg1', 'testArgument1']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc4', parsed_action_function.__name__)
        self.assertEqual(['testArgument1'], parsed_action_function_args)

        cli_arguments = ['testFunc5', 'ISS-1001']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc5', parsed_action_function.__name__)
        self.assertEqual(['ISS-1001', 'testArgument2Default'], parsed_action_function_args)

        cli_arguments = ['testFunc5', 'ISS-1001', '--arg2', 'testArgument2']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc5', parsed_action_function.__name__)
        self.assertEqual(['ISS-1001', 'testArgument2'], parsed_action_function_args)

        cli_arguments = ['testFunc6', '--arg2', 'testArgument2']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc6', parsed_action_function.__name__)
        self.assertEqual(['testArgument1Default', 'testArgument2'], parsed_action_function_args)

        cli_arguments = ['testFunc6', '--arg2', 'testArgument2', '--arg1', 'testArgument1']
        (parsed_action_function, parsed_action_function_args, parsed_global_arguments) = _parse_arguments(None, cli_arguments)
        self.assertEqual('testFunc6', parsed_action_function.__name__)
        self.assertEqual(['testArgument1', 'testArgument2'], parsed_action_function_args)






